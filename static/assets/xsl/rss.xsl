<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>"<xsl:value-of select="/rss/channel/title"/>" RSS Feed preview</title>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,shrink-to-fit=no" />
				<link href="/assets/css/readable.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<p style="line-height:0;font-size:0;color:transparent;">This is placeholder text, please ignore it.</p>
				<header>
					<h1><xsl:value-of select="/rss/channel/title"/></h1>
					<p>RSS Feed preview</p>
				</header>
				<nav>
					<span><a href="/">Home</a></span>
					<span><a href="https://benjaminhollon.com" target="blank">More from me</a></span>
				</nav>
				<main>
					<p>
						<xsl:value-of select="/rss/channel/description"/>
					</p>
					<p>To subscribe, just copy the URL of this page into an RSS feed reader.</p>
					<p style="text-align: center">
						<a hreflang="en" target="_blank">
							<xsl:attribute name="href">
								<xsl:value-of select="/rss/channel/link"/>
							</xsl:attribute>
							Visit Main Site &#x2192;
						</a>
					</p>
					<h2>Recent Posts</h2>
					<xsl:for-each select="/rss/channel/item">
						<article>
							<h3 style="text-align: center">
								<a target="_blank">
									<xsl:attribute name="href">
										<xsl:value-of select="link"/>
									</xsl:attribute>
									<xsl:value-of select="title"/>
								</a>
							</h3>
							<p style="text-align: center">
								Published:
								<time>
									<xsl:value-of select="pubDate" />
								</time>
							</p>
						</article>
					</xsl:for-each>
				</main>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
