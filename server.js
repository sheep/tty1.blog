import { Application } from "https://deno.land/x/abc@v1.3.3/mod.ts";
import nunjucks from "https://deno.land/x/nunjucks@3.2.3/mod.js";
import { renderMarkdown } from "https://deno.land/x/markdown_renderer/mod.ts";
import { load as yaml } from 'https://deno.land/x/js_yaml_port/js-yaml.js'
const decoder = new TextDecoder("utf-8");
import { parseFeed } from "https://deno.land/x/rss/mod.ts";

const markdownConfig = {
	allowedTags: ["iframe", "script", "style", "summary", "details"],
	allowedAttributes: {
		iframe: ["src", "style", "allowfullscreen", "width", "height", "class", "id"],
		script: ["src", "async"]
	},
	allowVulnerableTags: true
};

// Utility functions

// Based on https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
function encodeHTMLEntities(rawStr) {
	const encodedStr = rawStr.replace(/[\u00A0-\u9999<>\&]/g, function(i) {
		return '&#'+i.charCodeAt(0)+';';
	});
	return encodedStr;
}

const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
function encodePubDate(pubDate) {
	const date = new Date(pubDate);
	return `${weekday[date.getUTCDay()].slice(0, 3)}, ${date.getUTCDate()} ${month[date.getUTCMonth()].slice(0, 3)} ${date.getUTCFullYear()} ${date.getUTCHours() > 9 ? date.getUTCHours() : "0" + date.getUTCHours()}:${date.getUTCMinutes() > 9 ? date.getUTCMinutes() : "0" + date.getUTCMinutes()}:${date.getUTCSeconds() > 9 ? date.getUTCSeconds() : "0" + date.getUTCSeconds()} UTC`;
}

function readJSONSync(uri) {
	return JSON.parse(
		decoder.decode(
			Deno.readFileSync(uri)
		)
	);
}

// Config and Data
const config = readJSONSync('./config.json');

/*const blurbs = {};
for await (const blurb of Deno.readDir(config.data + "blurbs/")) {
	if (blurb.isFile && blurb.name.split('.')[1] === 'md') {
		const id = blurb.name.split('.')[0];
		const raw = decoder.decode(Deno.readFileSync(`${config.data}blurbs/${blurb.name}`));
		const split = raw.split('\n---\n');
		const metadata = yaml(split[0]);
		blurbs[id] = {...metadata};
		blurbs[id].id = id;
		blurbs[id].published = new Date(blurbs[id].published);
		blurbs[id].body = renderMarkdown(split.slice(1).join('\n---\n'), markdownConfig);
		// Time to read at 250 wpm; by all means not an accurate count, but it's something handy anyway
		blurbs[id].timeToRead = Math.round(blurbs[id].body.split(/\ |\n/g).length / 250);
	}
}*/

// Configure nunjucks
nunjucks.configure({ autoescape: true });

const app = new Application();

console.info(`http://localhost:${config.port}/`);

app.static("/", "static");

// 404
function error404() {
	return nunjucks.render('./templates/404.html', {
		title: "404 Not Found",
		subtitle: "I don't know where that page is but it isn't here",
		description: "This page does not exist."
	});
}

app.get("/", (c) => {
	return nunjucks.render('./templates/index.html', {
		title: "tty1",
		subtitle: "coming soon",
		description: "tty1 is a forthcoming blog by Benjamin Hollon about the terminal-only life.",
	})
});

app.get("/feed/", (c) => {
	c.response.headers = new Headers({
		'Content-type': 'text/xml'
	});

	const items = [
		{
			title: "Coming Soon",
			author: "Benjamin Hollon",
			pubDate: encodePubDate("2022-11-24 20:39:15 UTC"),
			link: "https://tty1.blog",
			description: renderMarkdown("tty1 is under construction and will begin publication at an undetermined date in the future. Thank you for your patience!")
		}
	];

	return nunjucks.render('./templates/feed.xml', {
		feed:	{
			title: "tty1",
			link: "https://tty1.blog",
			description: "A brand new blog, brought to you by popular demand! tty1's subject matter is probably the nerdiest out of any of my blogs: in tty1, I'll be writing about the process of me setting up and using a text-only operating system!",
			copyright: "All posts are usable under CC BY-SA 4.0.",
			language: "en-us",
			items
		}
	});
});

// It's hacky and doesn't work for sub-paths but seems to be the only way with abc right now
app.get("/:null/", (c) => {
	return error404();
});

app
	.start({ port: config.port });
